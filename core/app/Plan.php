<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault();
    }

    public function getUsersCountAttribute()
    {
        $planLogs = \App\PlanLog::where('plan_id', $this->id)->groupBy('user_id')->get();
        return $planLogs->count();
    }

    public function getAmountAttribute()
    {
        return round(\App\PoolLog::where('plan_id', $this->id)->sum('amount'), 3);
    }
}
