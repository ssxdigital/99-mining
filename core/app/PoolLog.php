<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoolLog extends Model
{
    public function getAmountAttribute($value)
    {
        return round($value, 3);
    }
}
