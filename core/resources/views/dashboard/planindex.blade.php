@extends('layouts.dashboard')

@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption uppercase">
                        <strong><i class="fa fa-info-circle"></i> {{ $page_title }}</strong>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <th> ID </th>
                            <th> Title </th>
                            <th> Price </th>
                            <th> Speed </th>
                            <th> Miner </th>
                            <th> Users Count </th>
                            <th> Amount </th>
                            <th> Add Pool </th>
                            <th> Pool History </th>
                            <th> Edit </th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($plans)

                            @foreach($plans as $plan)

                                <tr>
                                    <td> {{ $plan->id }} </td>
                                    <td> {{ $plan->title }} </td>
                                    <td> {{ $plan->price }} </td>
                                    <td> {{ $plan->speed }} </td>
                                    <td> {{ isset($plan->category)?$plan->category->name:'' }} </td>
                                    <td> {{ $plan->users_count }} </td>
                                    <td> {{ $plan->amount }} USD </td>
                                    <td>
                                        <button data-id="{{ $plan->id }}" data-title="{{ $plan->title }}" data-action="{{ route('plan.add.pool', $plan->id) }}" class="btn btn-success addpoolbtn" role="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <a href="{{ route('plan.pool.logs', $plan->id) }}" class="btn btn-default" role="button">
                                            <i class="fa fa-history"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('plan.edit', $plan->id) }}" class="btn btn-warning" role="button">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>

                            @endforeach

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- Add Pool Modal -->
    <div id="addpoolmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Pool</h4>
                </div>
                <form action="#" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p>Miner Name: </p>
                        <div class="form-group">
                            <b id="category_name"></b>
                        </div>
                        <p>Pool Amount: </p>
                        <div class="form-group">
                            <input type="text" id="pool_amount" name="pool_amount" placeholder="$ 1.234,567 USD" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add Pool</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <meta name="_token" content="{!! csrf_token() !!}" />
@endsection
@section('scripts')
<script src="{{ asset('assets/js/jquery.maskMoney.min.js') }}"></script>

<script>
    (function ($) {
        $(document).ready(function () {
            $('#pool_amount').maskMoney({
                prefix: '$ ',
                suffix: ' USD',
                thousands: '.',
                decimal: ',',
                precision: 3
            });
            
            $('.addpoolbtn').click(function (e) {
                var id = $(this).data('id');
                var name = $(this).data('name');
                var action = $(this).data('action')
                $('#category_name').html(name);
                $('#addpoolmodal form').attr('action', action)
                $('#addpoolmodal').modal();
            });
        });
    })(jQuery);
</script>
@endsection