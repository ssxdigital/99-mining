@extends('layouts.dashboard')

@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption uppercase">
                        <strong><i class="fa fa-info-circle"></i> {{ $page_title }}</strong>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <th> ID </th>
                            <th> Amount </th>
                            <th> Users Count </th>
                            <th> Amount per user </th> 
                        </tr>
                        </thead>
                        <tbody>

                        @if($poolLogs)

                            @foreach($poolLogs as $pool)

                                <tr>
                                    <td> {{ $pool->id }} </td>
                                    <td> $ {{ $pool->amount }} USD </td>
                                    <td> {{ $pool->users_count }} </td>
                                    <td> $ {{ round($pool->amount / $pool->users_count, 3) }} USD </td>
                                </tr>

                            @endforeach

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection